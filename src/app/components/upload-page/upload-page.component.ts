import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.css']
})
export class UploadPageComponent implements OnInit {

  constructor(){}

  ngOnInit() {}

  readAndProcessFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    reader.onload = (event) => { // called once readAsDataURL is completed
      this.process(reader.result);
    }
    reader.readAsArrayBuffer(file);
  }

  process(fileContent){
    var dv = new DataView(fileContent);
    var offset = 0, recess = 0;
    var pieces = [];
    var i = 0;
    if (dv.getUint16(offset) == 0xffd8){
        offset += 2;
        var app1 = dv.getUint16(offset);
        offset += 2;
        while (offset < dv.byteLength){
            if (app1 == 0xffe1){

                pieces[i] = {recess:recess,offset:offset-2};
                recess = offset + dv.getUint16(offset);
                i++;
            }
            else if (app1 == 0xffda){
                break;
            }
            offset += dv.getUint16(offset);
            var app1 = dv.getUint16(offset);
            offset += 2;
        }
        if (pieces.length > 0){
            var newPieces = [];
            pieces.forEach(function(v){
                newPieces.push(fileContent.slice(v.recess, v.offset));
            }, this);
            newPieces.push(fileContent.slice(recess));
            var br = new Blob(newPieces, {type: 'image/jpeg'});
            window.open(URL.createObjectURL(br), "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400");
        }
    }       
}
}
